from selenium import webdriver
from faker import Faker
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
import unittest
import time

fake = Faker()
base_url = "https://app.vyond.com"

class BaseTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path="C:\Drivers\chromedriver.exe")
        self.driver.maximize_window()
        self.driver.get(base_url)

    def tearDown(self):
        self.driver.close()
        self.driver.quit()

class register(BaseTest):
    def test_register_success(self):

        # Step 1 - Click Sign Up Button
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="app"]/div/div[1]/div[3]/ul/li[1]/a'))
            ).click()

        # Step 2 - Click Sign Up With Email
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/div[1]/button'))
            ).click()

        # Step 3 - Type First Name
            first_name = fake.first_name()
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.NAME, 'FIRST_NAME'))
            ).send_keys(first_name)

        # Step 4 - Type Last Name
            last_name = fake.last_name()
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.NAME, 'LAST_NAME'))
            ).send_keys(last_name)

        # Step 5 - Type Company
            company = fake.company()
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.NAME, 'COMPANY_NAME'))
            ).send_keys(company)

        # Step 6 - Select Job Role
            selectJob = Select(self.driver.find_element_by_name('JOB_ROLE_ID'))
            selectJob.select_by_visible_text("Other")

        # Step 7 - Type Role
            job = fake.job()
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.NAME, 'JOB_ROLE_NAME'))
            ).send_keys(job)

        # Step 8 - Type Work Phone
            phone = fake.phone_number()
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.ID, 'phone-text-field'))
            ).send_keys(phone)

        # Step 9 - Select Country
            selectCountry = Select(self.driver.find_element_by_name('COUNTRY'))
            selectCountry.select_by_visible_text("Zimbabwe")

        # Step 10 - Type Work Email
            email = fake.ascii_email()
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.NAME, 'WORK_EMAIL'))
            ).send_keys(email)

        # Step 11 - Type Password
            password = fake.password()
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.NAME, 'PASSWORD'))
            ).send_keys(password)

        # Step 12 - Click Agree Terms
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="app"]/div/div[2]/div[2]/div/form/div[9]/label/span/span[1]'))
            ).click()

            time.sleep(60)