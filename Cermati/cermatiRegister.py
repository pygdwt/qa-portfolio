from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from faker import Faker
import unittest
import time

fake = Faker(["id_ID"])
base_url = "https://www.cermati.com/gabung"

class BaseTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path="C:\Drivers\chromedriver.exe")
        self.driver.maximize_window()
        self.driver.get(base_url)

    def tearDown(self):
        self.driver.close()
        self.driver.quit()

class Register(BaseTest):

    def test_register_success(self):

        # Step 1 - Type Email
        email = fake.ascii_email()
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.NAME, 'email'))
        ).send_keys(email)

        # Step 2 - Type Password
        password = fake.password()
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.NAME, 'password'))
        ).send_keys(password)

        # Step 3 - Type Confirm Password
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.NAME, 'confirmPassword'))
        ).send_keys(password)

        # Step 4 - Type First Name
        firstName = fake.first_name()
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.NAME, 'firstName'))
        ).send_keys(firstName)

        # Step 5 - Type Last Name
        lastName = fake.last_name()
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.NAME, 'lastName'))
        ).send_keys(lastName)

        # Step 6 - Type Phone Number
        numberPhone = fake.phone_number()
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.NAME, 'mobilePhone'))
        ).send_keys(numberPhone)

        # Step 7 - Type Kota
        kota = fake.city()
        # selectKota = Select(self.driver.find_element_by_name('residenceCity'))
        # selectKota.select_by_value(kota)
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.NAME, 'residenceCity'))
        ).send_keys(kota)

        time.sleep(10)